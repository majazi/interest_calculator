#Programmer:        Mike N. Ajazi
#Date:              04/04/2020
#Purpose:           David wants a compound interest calculator

#for GUI
from tkinter import *
import tkinter
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from pandas import DataFrame
import tkinter as tk

def calculate(baseAmount, interestRate, compoundInterval, time, contributions):
#dropdown for compoundInterval
    # 1 = yearly
    # 12 = monthly
    #52 = weekly
    #365 = daily

    num = 0
    result = baseAmount*(1+interestRate/compoundInterval)**(compoundInterval)
    for num in range(0, time+1):
        result = baseAmount*(1+interestRate/compoundInterval)**(compoundInterval)
        baseAmount += contributions
        print(result)
    
# on change dropdown value
def change_dropdown(*args):
    print( tkvar.get() )

def getEntryValues():
    baseAmount = float(e1.get())
    interestRate = float(e2.get())
    time = int(e3.get())
    contributions = float(e4.get())
    
    calculate(baseAmount, interestRate, 12, time, contributions)
    
#calculate(100,.08,1,40)



root = Tk()
root.title("Ajazi Compound Interest Calculator")



# Add a grid
mainframe = Frame(root)
mainframe.grid(column=0,row=0, sticky=(N,W,E,S) )
mainframe.columnconfigure(0, weight = 1)
mainframe.rowconfigure(0, weight = 1)
mainframe.pack(pady = 50, padx = 50)



data2 = {'Year': [1920,1930,1940,1950,1960,1970,1980,1990,2000,2010],
         'Money': [9.8,12,8,14.2,17.9,17,26.5,36.2,55.5,76.3]
        }
df2 = DataFrame(data2,columns=['Year','Money'])


#root= tk.Tk() 

tk.Label(mainframe, text="Base Ammount: ").grid(sticky="E", row=0, column=0) #Grid
tk.Label(mainframe, text="Interest Rate: ").grid(sticky="E", row=1,column=0) #Grid
tk.Label(mainframe, text="Time: ").grid(sticky="E", row=2,column=0) #Grid
tk.Label(mainframe, text="Contributions: ").grid(sticky="E", row=3,column=0) #Grid

e1 = tk.Entry(mainframe)
e2 = tk.Entry(mainframe)
e3 = tk.Entry(mainframe)
e4 = tk.Entry(mainframe)

e1.grid(row=0, column=1) #Grid
e2.grid(row=1, column=1) #Grid
e3.grid(row=2, column=1) #Grid
e4.grid(row=3, column=1) #Grid

###############################################################


figure2 = plt.Figure(figsize=(5,4), dpi=100)
ax2 = figure2.add_subplot(111)
line2 = FigureCanvasTkAgg(figure2, mainframe)
line2.get_tk_widget().grid(row=5, column=5) #Grid
df2 = df2[['Year','Money']].groupby('Year').sum()
df2.plot(kind='line', legend=True, ax=ax2, color='r',marker='o', fontsize=10)
ax2.set_title('Year Vs. Growth')


################################################################
#Start Dropdown
# Create a Tkinter variable
tkvar = StringVar(root)

# Dictionary with options
choices = { 'Yearly','Monthly','Weekly','Daily'}
tkvar.set('Monthly') # set the default option

popupMenu = OptionMenu(mainframe, tkvar, *choices)
Label(mainframe, text="Choose Compound Interval: ").grid(sticky="E", row = 4, column = 0) #Grid
popupMenu.grid(row = 4, column =1) #Grid



# link function to change dropdown
tkvar.trace('w', change_dropdown)
#tkvar.trace('w', e1)
#End Dropdown
###############################################################

#command=some_function
tk.Button(mainframe, 
          text='Calculate', 
          command=getEntryValues).grid(sticky="E", row=5, column=0, pady=40) #Grid

root.mainloop()












