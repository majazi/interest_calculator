﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterestCalculator
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();           
        }

        private void btn_calculate_Click(object sender, EventArgs e)
        {
            double n = 0.0;

            string length = cmb_length.Text; 
            int time = Int32.Parse(txt_length.Text); 
            string contributionsPer = cmb_contributionsPer.Text; 
            double currentBalence = double.Parse(txt_currentBalence.Text); 
            double interestRate = double.Parse(txt_interestRate.Text); 
            double contributions = double.Parse(txt_Contributions.Text);
            string compoundFreq = cmb_compoundFreq.Text;
            double contributionCount = 0.0;

            var iterationList = new List<double>();

            //TODO display error message on UI if else statemetns occur
            
            switch (length)
            {
                case "Years":
                    if (compoundFreq == "Annually")
                    {
                        n = 1;
                    }
                    else if (compoundFreq == "Monthly")
                    {
                        n = 12;
                    }
                    else if (compoundFreq == "Weekly")
                    {
                        n = 52;
                    }
                    else if (compoundFreq == "Daily")
                    {
                        n = 365;
                    }
                    else if (compoundFreq == "Hourly")
                    {
                        n = 8760;
                    }
                    else
                    {
                        Console.WriteLine("ERROR IN Years compoundFreq");
                    }

                    if (contributionsPer == "Year")
                    {
                        contributionCount = 1;
                    }
                    else if (contributionsPer == "Month")
                    {
                        contributionCount = 12;
                    }
                    else if (contributionsPer == "Week")
                    {
                        contributionCount = 52;
                    }                    
                    else if (contributionsPer == "Day")
                    {
                        contributionCount = 365;
                    }
                    else if (contributionsPer == "Hour")
                    {
                        contributionCount = 8760;
                    }
                    else
                    {
                        Console.WriteLine("ERROR IN Years contributionsPer");
                    }
                    break;                    

                case "Months":
                    if (compoundFreq == "Annually")
                    {
                        n = 0;
                    }
                    else if (compoundFreq == "Monthly")
                    {
                        n = 1;
                    }
                    else if (compoundFreq == "Weekly")
                    {
                        n = 4.34524;
                    }
                    else if (compoundFreq == "Daily")
                    {
                        n = 30.4167;
                    }
                    else if (compoundFreq == "Hourly")
                    {
                        n = 730.001;
                    }
                    else
                    {
                        Console.WriteLine("ERROR IN Months");
                    }

                    if (contributionsPer == "Year")
                    {
                        contributionCount = 0;
                    }
                    else if (contributionsPer == "Month")
                    {
                        contributionCount = 1;
                    }
                    else if (contributionsPer == "Week")
                    {
                        contributionCount = 4.34524;
                    }
                    else if (contributionsPer == "Day")
                    {
                        contributionCount = 30.4167;
                    }
                    else if (contributionsPer == "Hour")
                    {
                        contributionCount = 730.001;
                    }
                    else
                    {
                        Console.WriteLine("ERROR IN Years contributionsPer");
                    }
                    break;

                case "Weeks":
                    if (compoundFreq == "Annually")
                    {
                        n = 0;
                    }
                    else if (compoundFreq == "Monthly")
                    {
                        n = 0;
                    }
                    else if (compoundFreq == "Weekly")
                    {
                        n = 1;
                    }
                    else if (compoundFreq == "Daily")
                    {
                        n = 7;
                    }
                    else if (compoundFreq == "Hourly")
                    {
                        n = 168;
                    }
                    else
                    {
                        Console.WriteLine("ERROR IN Weeks");
                    }

                    if (contributionsPer == "Year")
                    {
                        contributionCount = 0;
                    }
                    else if (contributionsPer == "Month")
                    {
                        contributionCount = 0;
                    }
                    else if (contributionsPer == "Week")
                    {
                        contributionCount = 1;
                    }
                    else if (contributionsPer == "Day")
                    {
                        contributionCount = 7;
                    }
                    else if (contributionsPer == "Hour")
                    {
                        contributionCount = 168;
                    }
                    else
                    {
                        Console.WriteLine("ERROR IN Years contributionsPer");
                    }
                    break;

                case "Days":
                    if (compoundFreq == "Annually")
                    {
                        n = 0;
                    }
                    else if (compoundFreq == "Monthly")
                    {
                        n = 0;
                    }
                    else if (compoundFreq == "Weekly")
                    {
                        n = 0;
                    }
                    else if (compoundFreq == "Daily")
                    {
                        n = 1;
                    }
                    else if (compoundFreq == "Hourly")
                    {
                        n = 24;
                    }
                    else
                    {
                        Console.WriteLine("ERROR IN Months");
                    }

                    if (contributionsPer == "Year")
                    {
                        contributionCount = 0;
                    }
                    else if (contributionsPer == "Month")
                    {
                        contributionCount = 0;
                    }
                    else if (contributionsPer == "Week")
                    {
                        contributionCount = 0;
                    }
                    else if (contributionsPer == "Day")
                    {
                        contributionCount = 1;
                    }
                    else if (contributionsPer == "Hour")
                    {
                        contributionCount = 24;
                    }
                    else
                    {
                        Console.WriteLine("ERROR IN Years contributionsPer");
                    }
                    break;

                case "Hours":
                    if (compoundFreq == "Annually")
                    {
                        n = 0;
                    }
                    else if (compoundFreq == "Monthly")
                    {
                        n = 0;
                    }
                    else if (compoundFreq == "Weekly")
                    {
                        n = 0;
                    }
                    else if (compoundFreq == "Daily")
                    {
                        n = 0;
                    }
                    else if (compoundFreq == "Hourly")
                    {
                        n = 1;
                    }
                    else
                    {
                        Console.WriteLine("ERROR IN Hours");
                    }
                    if (contributionsPer == "Year")
                    {
                        contributionCount = 0;
                    }
                    else if (contributionsPer == "Month")
                    {
                        contributionCount = 0;
                    }
                    else if (contributionsPer == "Week")
                    {
                        contributionCount = 0;
                    }
                    else if (contributionsPer == "Day")
                    {
                        contributionCount = 0;
                    }
                    else if (contributionsPer == "Hour")
                    {
                        contributionCount = 1;
                    }
                    else
                    {
                        Console.WriteLine("ERROR IN Years contributionsPer");
                    }
                    break;
                default:

                    break;
            }

            double result = currentBalence;
            cht_main.Series.Clear();
            cht_main.Series.Add("mainChart");
            cht_main.ChartAreas[0].AxisX.Minimum = 1;
            cht_main.ChartAreas[0].AxisY.LabelStyle.Format = "${###,###}";
            cht_main.Series["mainChart"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            cht_main.Series["mainChart"].BorderWidth = 5;
            cht_main.Series["mainChart"].Color = Color.Green;

            int iterrationCounter = 1;
            txt_main.Text = "";
            for (int i = 0; i < time; i++)
            {  
                result = Statistics.compoundInterestCalculator(result, interestRate, n);
                result += contributions * contributionCount; //TODO add ability to choose to either contribute at beginning or end
                
                iterationList.Add(result);
                
                cht_main.Series["mainChart"].Points.AddXY(iterrationCounter, result);

                txt_main.Text += iterrationCounter + " --------- " +  string.Format("${0:###,###.00}", result) + "\r\n";

                iterrationCounter += 1;
            }
            //Console.WriteLine(iterationList[-1]);
            Console.WriteLine(result);
        }

        private void btn_reset_Click(object sender, EventArgs e)
        {
            txt_currentBalence.Text = "100";
            txt_Contributions.Text = "100";
            cmb_contributionsPer.Text = "Month";
            txt_length.Text = "10";
            cmb_length.Text = "Years";
            txt_interestRate.Text = ".1";
            cmb_compoundFreq.Text = "Annually";
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

    }
}
