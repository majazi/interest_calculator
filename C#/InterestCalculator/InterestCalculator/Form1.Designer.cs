﻿namespace InterestCalculator
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txt_main = new System.Windows.Forms.TextBox();
            this.cht_main = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.S = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.cmb_contributionsPer = new System.Windows.Forms.ComboBox();
            this.btn_reset = new System.Windows.Forms.Button();
            this.lbl_investment = new System.Windows.Forms.Label();
            this.cmb_compoundFreq = new System.Windows.Forms.ComboBox();
            this.txt_currentBalence = new System.Windows.Forms.TextBox();
            this.btn_calculate = new System.Windows.Forms.Button();
            this.txt_Contributions = new System.Windows.Forms.TextBox();
            this.lbl_compoundFreq = new System.Windows.Forms.Label();
            this.lbl_Contribuitions = new System.Windows.Forms.Label();
            this.lbl_interestRate = new System.Windows.Forms.Label();
            this.txt_length = new System.Windows.Forms.TextBox();
            this.txt_interestRate = new System.Windows.Forms.TextBox();
            this.lbl_length = new System.Windows.Forms.Label();
            this.cmb_length = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cht_main)).BeginInit();
            this.S.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.cht_main);
            this.panel1.Controls.Add(this.S);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(788, 495);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txt_main);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(271, 288);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(517, 207);
            this.panel3.TabIndex = 1;
            // 
            // txt_main
            // 
            this.txt_main.Location = new System.Drawing.Point(3, 3);
            this.txt_main.Multiline = true;
            this.txt_main.Name = "txt_main";
            this.txt_main.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_main.Size = new System.Drawing.Size(520, 194);
            this.txt_main.TabIndex = 2;
            // 
            // cht_main
            // 
            chartArea1.Name = "ChartArea1";
            this.cht_main.ChartAreas.Add(chartArea1);
            this.cht_main.Location = new System.Drawing.Point(274, 12);
            this.cht_main.Name = "cht_main";
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            this.cht_main.Series.Add(series1);
            this.cht_main.Size = new System.Drawing.Size(511, 223);
            this.cht_main.TabIndex = 0;
            this.cht_main.Text = "chart1";
            // 
            // S
            // 
            this.S.Controls.Add(this.tabControl1);
            this.S.Dock = System.Windows.Forms.DockStyle.Left;
            this.S.Location = new System.Drawing.Point(0, 0);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(271, 495);
            this.S.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(262, 471);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.cmb_contributionsPer);
            this.tabPage1.Controls.Add(this.btn_reset);
            this.tabPage1.Controls.Add(this.lbl_investment);
            this.tabPage1.Controls.Add(this.cmb_compoundFreq);
            this.tabPage1.Controls.Add(this.txt_currentBalence);
            this.tabPage1.Controls.Add(this.btn_calculate);
            this.tabPage1.Controls.Add(this.txt_Contributions);
            this.tabPage1.Controls.Add(this.lbl_compoundFreq);
            this.tabPage1.Controls.Add(this.lbl_Contribuitions);
            this.tabPage1.Controls.Add(this.lbl_interestRate);
            this.tabPage1.Controls.Add(this.txt_length);
            this.tabPage1.Controls.Add(this.txt_interestRate);
            this.tabPage1.Controls.Add(this.lbl_length);
            this.tabPage1.Controls.Add(this.cmb_length);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(254, 445);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Interest";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(171, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "per";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmb_contributionsPer
            // 
            this.cmb_contributionsPer.DisplayMember = "Months";
            this.cmb_contributionsPer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_contributionsPer.FormattingEnabled = true;
            this.cmb_contributionsPer.Items.AddRange(new object[] {
            "Hour",
            "Day",
            "Week",
            "Month",
            "Year"});
            this.cmb_contributionsPer.Location = new System.Drawing.Point(195, 39);
            this.cmb_contributionsPer.Name = "cmb_contributionsPer";
            this.cmb_contributionsPer.Size = new System.Drawing.Size(53, 21);
            this.cmb_contributionsPer.TabIndex = 18;
            this.cmb_contributionsPer.Tag = "Months";
            // 
            // btn_reset
            // 
            this.btn_reset.Location = new System.Drawing.Point(82, 305);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(91, 31);
            this.btn_reset.TabIndex = 17;
            this.btn_reset.Text = "Reset";
            this.btn_reset.UseVisualStyleBackColor = true;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // lbl_investment
            // 
            this.lbl_investment.AutoSize = true;
            this.lbl_investment.Location = new System.Drawing.Point(30, 17);
            this.lbl_investment.Name = "lbl_investment";
            this.lbl_investment.Size = new System.Drawing.Size(86, 13);
            this.lbl_investment.TabIndex = 0;
            this.lbl_investment.Text = "Current Balence:";
            this.lbl_investment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmb_compoundFreq
            // 
            this.cmb_compoundFreq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_compoundFreq.FormattingEnabled = true;
            this.cmb_compoundFreq.Items.AddRange(new object[] {
            "Annually",
            "Monthly",
            "Weekly",
            "Daily",
            "Hourly"});
            this.cmb_compoundFreq.Location = new System.Drawing.Point(122, 121);
            this.cmb_compoundFreq.Name = "cmb_compoundFreq";
            this.cmb_compoundFreq.Size = new System.Drawing.Size(121, 21);
            this.cmb_compoundFreq.TabIndex = 15;
            // 
            // txt_currentBalence
            // 
            this.txt_currentBalence.Location = new System.Drawing.Point(122, 14);
            this.txt_currentBalence.Name = "txt_currentBalence";
            this.txt_currentBalence.Size = new System.Drawing.Size(126, 20);
            this.txt_currentBalence.TabIndex = 4;
            // 
            // btn_calculate
            // 
            this.btn_calculate.Location = new System.Drawing.Point(59, 231);
            this.btn_calculate.Name = "btn_calculate";
            this.btn_calculate.Size = new System.Drawing.Size(134, 49);
            this.btn_calculate.TabIndex = 16;
            this.btn_calculate.Text = "Calculate";
            this.btn_calculate.UseVisualStyleBackColor = true;
            this.btn_calculate.Click += new System.EventHandler(this.btn_calculate_Click);
            // 
            // txt_Contributions
            // 
            this.txt_Contributions.Location = new System.Drawing.Point(122, 40);
            this.txt_Contributions.Name = "txt_Contributions";
            this.txt_Contributions.Size = new System.Drawing.Size(45, 20);
            this.txt_Contributions.TabIndex = 6;
            // 
            // lbl_compoundFreq
            // 
            this.lbl_compoundFreq.AutoSize = true;
            this.lbl_compoundFreq.Location = new System.Drawing.Point(2, 121);
            this.lbl_compoundFreq.Name = "lbl_compoundFreq";
            this.lbl_compoundFreq.Size = new System.Drawing.Size(114, 13);
            this.lbl_compoundFreq.TabIndex = 14;
            this.lbl_compoundFreq.Text = "Compound Frequency:";
            this.lbl_compoundFreq.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_Contribuitions
            // 
            this.lbl_Contribuitions.AutoSize = true;
            this.lbl_Contribuitions.Location = new System.Drawing.Point(45, 43);
            this.lbl_Contribuitions.Name = "lbl_Contribuitions";
            this.lbl_Contribuitions.Size = new System.Drawing.Size(71, 13);
            this.lbl_Contribuitions.TabIndex = 7;
            this.lbl_Contribuitions.Text = "Contributions:";
            this.lbl_Contribuitions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_interestRate
            // 
            this.lbl_interestRate.AutoSize = true;
            this.lbl_interestRate.Location = new System.Drawing.Point(0, 95);
            this.lbl_interestRate.Name = "lbl_interestRate";
            this.lbl_interestRate.Size = new System.Drawing.Size(116, 13);
            this.lbl_interestRate.TabIndex = 12;
            this.lbl_interestRate.Text = "Interest Rate (decimal):";
            this.lbl_interestRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_length
            // 
            this.txt_length.Location = new System.Drawing.Point(122, 66);
            this.txt_length.Name = "txt_length";
            this.txt_length.Size = new System.Drawing.Size(45, 20);
            this.txt_length.TabIndex = 8;
            // 
            // txt_interestRate
            // 
            this.txt_interestRate.Location = new System.Drawing.Point(122, 92);
            this.txt_interestRate.Name = "txt_interestRate";
            this.txt_interestRate.Size = new System.Drawing.Size(126, 20);
            this.txt_interestRate.TabIndex = 11;
            // 
            // lbl_length
            // 
            this.lbl_length.AutoSize = true;
            this.lbl_length.Location = new System.Drawing.Point(83, 69);
            this.lbl_length.Name = "lbl_length";
            this.lbl_length.Size = new System.Drawing.Size(33, 13);
            this.lbl_length.TabIndex = 9;
            this.lbl_length.Text = "Time:";
            this.lbl_length.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmb_length
            // 
            this.cmb_length.DisplayMember = "Months";
            this.cmb_length.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_length.FormattingEnabled = true;
            this.cmb_length.Items.AddRange(new object[] {
            "Hours",
            "Days",
            "Weeks",
            "Months",
            "Years"});
            this.cmb_length.Location = new System.Drawing.Point(178, 65);
            this.cmb_length.Name = "cmb_length";
            this.cmb_length.Size = new System.Drawing.Size(70, 21);
            this.cmb_length.TabIndex = 10;
            this.cmb_length.Tag = "Months";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.textBox3);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.comboBox3);
            this.tabPage2.Controls.Add(this.textBox4);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.comboBox1);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.textBox5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(254, 445);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Time";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Desired Amount:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(121, 26);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(122, 20);
            this.textBox3.TabIndex = 36;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(170, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "per";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBox3
            // 
            this.comboBox3.DisplayMember = "Months";
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Hour",
            "Day",
            "Month",
            "Year"});
            this.comboBox3.Location = new System.Drawing.Point(195, 78);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(47, 21);
            this.comboBox3.TabIndex = 33;
            this.comboBox3.Tag = "Months";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(121, 78);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(45, 20);
            this.textBox4.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(44, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Contributions:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(81, 305);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 31);
            this.button1.TabIndex = 30;
            this.button1.Text = "Reset";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Current Balence:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Annually",
            "Monthy",
            "Daily"});
            this.comboBox1.Location = new System.Drawing.Point(121, 136);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 28;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(121, 52);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(122, 20);
            this.textBox2.TabIndex = 19;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(58, 231);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(134, 49);
            this.button2.TabIndex = 29;
            this.button2.Text = "Calculate";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Compound Frequency:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-1, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Interest Rate (decimal):";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(121, 107);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(122, 20);
            this.textBox5.TabIndex = 25;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(800, 495);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Main";
            this.Text = "Ajazi Interest Calculator";
            this.Load += new System.EventHandler(this.Main_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cht_main)).EndInit();
            this.S.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel S;
        private System.Windows.Forms.ComboBox cmb_length;
        private System.Windows.Forms.Label lbl_length;
        private System.Windows.Forms.TextBox txt_length;
        private System.Windows.Forms.Label lbl_Contribuitions;
        private System.Windows.Forms.TextBox txt_Contributions;
        private System.Windows.Forms.Label lbl_investment;
        private System.Windows.Forms.TextBox txt_currentBalence;
        private System.Windows.Forms.ComboBox cmb_compoundFreq;
        private System.Windows.Forms.Label lbl_compoundFreq;
        private System.Windows.Forms.Label lbl_interestRate;
        private System.Windows.Forms.TextBox txt_interestRate;
        private System.Windows.Forms.TextBox txt_main;
        private System.Windows.Forms.DataVisualization.Charting.Chart cht_main;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.Button btn_calculate;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.ComboBox cmb_contributionsPer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label7;
    }
}

