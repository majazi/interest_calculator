﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterestCalculator
{
    class Statistics
    {
        public static double compoundInterestCalculator(double currentBalence, double interestRate, double n, int time=1)
        {
            double result = 0.0;

            result = currentBalence * (Math.Pow(1 + interestRate / n,(n*time)));

            return result;
        }
    }
}
